FROM node:12.11.1-buster-slim AS base

WORKDIR /app

#
# ---- Dependencies ----
FROM base AS dependencies

COPY package.json .
COPY package-lock.json .

RUN npm install --production
RUN cp -R node_modules prod_node_modules
RUN npm install

# copy source
COPY . .

#
# ---- Test ----
# run linters, setup, build and tests
FROM dependencies AS test

RUN npm test

#
# ---- Release ----
FROM base AS release
# copy production node_modules
COPY --from=dependencies /app/prod_node_modules ./node_modules
COPY --from=dependencies /app/index.js ./index.js
COPY --from=dependencies /app/package.json ./package.json

CMD [ "npm", "start" ]

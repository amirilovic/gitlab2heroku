# Deploying Docker Image to Heroku From Gitlab CI/CD

Create a repo on Gitlab called

Clone the repo

Create an account at heroku.com

Install heroku cli

```bash
$ brew tap heroku/brew && brew install heroku
```

Login with cli

```bash
$ heroku login
```

Create new heroku app:

```bash
$ heroku apps:create gitlab2heroku
```

Generate heroku token for Gitlab CI/CD

```bash
$ heroku auth:token
```

Copy the token and create CI/CD variable named `HEROKU_TOKEN` on Gitlab repo > Settings > CI/CD > Variables

Create `.gitlab-ci.yml` file:

```yaml
image: docker:latest
services:
  - docker:dind

variables:
  DOCKER_DRIVER: overlay

stages:
  - build

docker-build:
  stage: build
  script:
    - docker build -f Dockerfile -t registry.heroku.com/gitlab2heroku .
    - docker login -u _ -p $HEROKU_TOKEN registry.heroku.com
    - docker push registry.heroku.com/gitlab2heroku/web
    - export IMAGE_ID=$(docker inspect registry.heroku.com/gitlab2heroku/web --format={{.Id}})
    - apk add --no-cache curl
    - echo "Docker Image ID is $IMAGE_ID"
    - |-
      curl -X PATCH https://api.heroku.com/apps/gitlab2heroku/formation --header "Content-Type: application/json" --header "Accept: application/vnd.heroku+json; version=3.docker-releases" --header "Authorization: Bearer ${HEROKU_TOKEN}" --data '{ "updates": [ { "type": "web", "docker_image": "'${IMAGE_ID}'" } ] }'
```

Create `Dockerfile`

Commit and push changes, you should see build immediately triggered under CI/CD > Pipelines 🚀
